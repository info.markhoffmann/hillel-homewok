function numbers(begin, end) {
    for (let i = begin; i <= end; i++) {
        let res = '';
        if (i % 3 === 0) {
            res += 'Fizz';
        }
        if (i % 5 === 0) {
            res += 'Buzz';
        }
        res = res || i;

        console.log(res);
    }
}

numbers(0, 50)
