let num = 0;

for (let i = 1; i < 10; i++) {
    for (let j = 1; j < 10; j++) {
        num = i * j;

        if (num / 10 < 1) {
            num = +i * j;
        }

        if (j <= i) {
            console.log(j + "*" + i + "=" + num);
        }
    }

}
