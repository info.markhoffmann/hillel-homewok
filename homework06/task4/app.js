let isWeekend = function (date1) {
    let dt = new Date(date1);

    if (dt.getDay() === 6 || dt.getDay() === 0) {
        return "выходной";
    } else {
        return "не выходной";
    }
}

console.log(isWeekend('Nov 16, 2020'));
