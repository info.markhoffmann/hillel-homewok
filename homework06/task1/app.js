function getDateWithHyphen (hyphen) {
    let year = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let date = new Date().getDate();

    return year + hyphen + month + hyphen + date
}

console.log(getDateWithHyphen('-'))
