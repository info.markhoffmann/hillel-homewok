function getNubers(n){
    let numbers = []
    for (let i = 1; i < n; i++) {
        if (i % 2 !== 0) {
            numbers.push(i)
        }
    }
    return numbers.join(',')
}

console.log(getNubers(7))
