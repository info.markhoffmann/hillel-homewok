const isPlainObject = (element) => typeof element === 'object' && !Array.isArray(element) && element !== null;

const data = {element: 1};

console.log(isPlainObject(data))

