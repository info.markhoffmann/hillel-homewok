let submitBtn = document.getElementById('submit');
let input = document.getElementById('input');

submitBtn.addEventListener('click', function () {
    if (isNaN(input.value)) {
        alert(input.value + ' is not a number');
        input.value = '';
    } else {
        alert(input.value + ' is a number')
        input.value = '';
    }
})
